find_file(BuildboxBenchmarkSetup BuildboxBenchmarkSetup.cmake HINTS ${BuildboxCommon_DIR})
include(${BuildboxBenchmarkSetup})

# Every directory containing a header file needed by the benchmarks, must be added here.
include_directories(. ../buildboxcasd)

add_executable(local_benchmark local_benchmark.cpp)
target_link_libraries(local_benchmark casd ${BENCHMARK_TARGET})

add_executable(fslocalcas_benchmark fslocalcas_benchmark.cpp)
target_link_libraries(fslocalcas_benchmark casd ${BENCHMARK_TARGET})
