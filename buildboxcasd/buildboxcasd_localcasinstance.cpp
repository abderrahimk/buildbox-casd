/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcasinstance.h>

#include <buildboxcasd_metricnames.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_timeutils.h>
#include <buildboxcommonmetrics_countingmetricutil.h>
#include <buildboxcommonmetrics_countingmetricvalue.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>

using namespace buildboxcasd;
using namespace buildboxcommon;

/*
 *   Helper method that stores err in status, and logs the message.
 */
void logAndStoreMessage(const grpc::StatusCode &code, const std::string &err,
                        google::rpc::Status *status)
{
    status->set_message(err);
    status->set_code(code);
    BUILDBOX_LOG_ERROR(err);
}

LocalCasInstance::LocalCasInstance(std::shared_ptr<LocalCas> storage,
                                   std::shared_ptr<FileStager> file_stager,
                                   const std::string &instance_name)
    : CasInstance(instance_name), d_storage(storage),
      d_file_stager(file_stager)
{
}

grpc::Status
LocalCasInstance::FindMissingBlobs(const FindMissingBlobsRequest &request,
                                   FindMissingBlobsResponse *response)
{
    // Returns a list of digests that are *not* in the CAS.
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::FindMissingBlobs request for instance name \""
        << request.instance_name() << "\" for "
        << request.blob_digests().size() << " digest(s)");

    for (const Digest &digest : request.blob_digests()) {
        bool blob_in_cas = false;

        try {
            blob_in_cas = d_storage->hasBlob(digest);
        }
        catch (const std::runtime_error &) {
            BUILDBOX_LOG_ERROR("Could not determine if "
                               << digest.hash() << "is in local CAS.");
        }

        if (!blob_in_cas) {
            Digest *entry = response->add_missing_blob_digests();
            entry->CopyFrom(digest);
        }
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_FIND_MISSING,
                            request.blob_digests().size());

    return grpc::Status::OK;
}

grpc::Status
LocalCasInstance::BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                   BatchUpdateBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::BatchUpdateBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.requests().size()
        << " blob(s)");

    for (const auto &blob : request.requests()) {
        const google::rpc::Status status =
            writeToLocalStorage(blob.digest(), blob.data());

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(blob.digest());
        entry->mutable_status()->CopyFrom(status);
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_BATCH_UPDATE,
                            request.requests().size());

    return grpc::Status::OK;
}

grpc::Status
LocalCasInstance::BatchReadBlobs(const BatchReadBlobsRequest &request,
                                 BatchReadBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::BatchReadBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.digests().size()
        << " digest(s)");

    for (const Digest &digest : request.digests()) {
        std::string data;
        const google::rpc::Status status = readFromLocalStorage(digest, &data);

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(digest);
        entry->mutable_status()->CopyFrom(status);
        entry->set_data(data);
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_BATCH_READ,
                            response->responses_size());

    return grpc::Status::OK;
}

grpc::Status LocalCasInstance::Write(WriteRequest *request_message,
                                     ServerReader<WriteRequest> &request,
                                     WriteResponse *response,
                                     Digest *requested_digest)
{
    response->set_committed_size(0);

    const auto buffer_file = d_storage->createTemporaryFile();

    Digest digest_to_write;
    const auto request_status = CasInstance::processWriteRequest(
        request_message, request, &digest_to_write, buffer_file.name());

    if (request_status.error_code() != grpc::INVALID_ARGUMENT) {
        *requested_digest = digest_to_write;
    }

    if (!request_status.ok()) {
#ifdef BUILDBOX_CASD_BYTESTREAM_WRITE_RETURN_EARLY
        /* This is made conditional on gRPC's version due to a bug that
         * prevented clients from being notitied that a stream is half-closed:
         * https://github.com/grpc/grpc/pull/22668
         */

        if (request_status.error_code() == grpc::StatusCode::ALREADY_EXISTS) {
            // The blob is already present in the CAS. In that case, according
            // to the REAPI spec:
            // " [...] if another client has already completed the upload
            // [...], the request will terminate immediately with a response
            // whose `committed_size` is the full size of the uploaded file
            // (regardless of how much data was transmitted by the client)"
            response->set_committed_size(digest_to_write.size_bytes());
            return grpc::Status::OK;
        }
#endif

        return request_status;
    }

    // We can move the file directly to the CAS, avoiding copies:
    const auto move_status =
        moveTemporaryFileToLocalStorage(digest_to_write, buffer_file.name());

    if (move_status.ok()) {
        response->set_committed_size(digest_to_write.size_bytes());
    }

    return move_status;
}

google::rpc::Status
LocalCasInstance::writeToLocalStorage(const Digest &digest,
                                      const std::string data)
{
    google::rpc::Status status;

    try {
        d_storage->writeBlob(digest, data);
        status.set_code(grpc::StatusCode::OK);
    }
    catch (const std::invalid_argument &) {
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        status.set_message(
            "The size of the data does not match the size defined "
            "in the digest.");
    }
    catch (const std::runtime_error &e) {
        status.set_code(grpc::StatusCode::INTERNAL);
        status.set_message("Internal error while writing blob to local CAS: " +
                           std::string(e.what()));
    }

    return status;
}

grpc::Status
LocalCasInstance::moveTemporaryFileToLocalStorage(const Digest &digest,
                                                  const std::string &path)
{
    try {
        d_storage->moveBlobFromTemporaryFile(digest, path);
        return grpc::Status::OK;
    }
    catch (const std::invalid_argument &e) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "Invalid argument: " + std::string(e.what()));
    }
    catch (const std::runtime_error &e) {
        return grpc::Status(grpc::StatusCode::INTERNAL,
                            "Internal error while moving blob to local CAS: " +
                                std::string(e.what()));
    }
}

google::rpc::Status
LocalCasInstance::readFromLocalStorage(const Digest &digest,
                                       std::string *data) const
{
    return readFromLocalStorage(digest, data, 0, 0);
}

google::rpc::Status
LocalCasInstance::readFromLocalStorage(const Digest &digest, std::string *data,
                                       size_t offset, size_t limit) const
{
    google::rpc::Status status;

    try {
        const size_t read_length = (limit > 0) ? limit : LocalCas::npos;
        const auto data_ptr = d_storage->readBlob(digest, offset, read_length);
        if (data_ptr == nullptr) {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob not found in the local CAS.");
        }
        else {
            *data = std::move(*data_ptr);
            status.set_code(grpc::StatusCode::OK);
        }
    }
    catch (const std::out_of_range &) {
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        status.set_message("Read interval is out of range.");
    }
    catch (const std::runtime_error &e) {
        status.set_code(grpc::StatusCode::INTERNAL);
        status.set_message(
            "Internal error while fetching blob in local CAS: " +
            std::string(e.what()));
    }

    return status;
}

bool LocalCasInstance::hasBlob(const Digest &digest)
{
    return d_storage->hasBlob(digest);
}

google::rpc::Status LocalCasInstance::readBlob(const Digest &digest,
                                               std::string *data,
                                               size_t read_offset,
                                               size_t read_limit)
{
    return readFromLocalStorage(digest, data, read_offset, read_limit);
}

google::rpc::Status LocalCasInstance::writeBlob(const Digest &digest,
                                                const std::string &data)
{
    BUILDBOX_LOG_INFO("LocalCasInstance::writeBlob: writing digest of "
                      << digest.size_bytes() << " bytes");
    return writeToLocalStorage(digest, data);
}

Status LocalCasInstance::StageTree(
    const StageTreeRequest &stage_request,
    ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
{
    // Before we can stage the tree, we need to make sure that all of its
    // contents are stored locally.
    const auto tree_availability_status =
        prepareTreeForStaging(stage_request.root_digest());
    if (!tree_availability_status.ok()) {
        return tree_availability_status;
    }
    // All the blobs required to stage are present in the local storage.

    /* There are 3 cases for `stage_request.path()`:
     *  a) Empty string (we need to create a temporary directory),
     *  b) A path to a directory that doesn't exist yet
     *  (and that will be created by the stager), or
     *  c) A path to an empty directory.
     *
     * In cases a) and b), cleanup involves deleting the directory. But for c)
     * we just want to empty it.
     */

    bool stage_directory_needs_deleting = false;
    std::string stage_path;
    if (stage_request.path().empty()) {
        try {
            stage_path = createStagingDirectory();
            stage_directory_needs_deleting = true;
        }
        catch (const std::system_error &e) {
            std::ostringstream error_message;
            error_message << "Could not create temporary directory to stage: "
                          << stage_request.root_digest() << ": " << e.what();

            BUILDBOX_LOG_ERROR(error_message.str());
            return Status(grpc::StatusCode::INTERNAL, error_message.str());
        }
    }
    else {
        stage_path = stage_request.path();

        const bool stage_directory_exists =
            buildboxcommon::FileUtils::isDirectory(
                stage_request.path().c_str());

        // If the directory does not exist, we create it and will need
        // to delete it later:
        if (!stage_directory_exists) {
            buildboxcommon::FileUtils::createDirectory(
                stage_request.path().c_str());
            stage_directory_needs_deleting = true;
        }
    }

    // Stage the files with the stage method which will also write back
    // onto the stream to the client to inform them that the staging is
    // done.
    Status stage_status;
    std::unique_ptr<FileStager::StagedDirectory> staged_directory;
    {
        buildboxcommon::buildboxcommonmetrics::MetricGuard<
            buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
            mt(MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_STAGE);

        stage_status =
            stage(stage_request.root_digest(), stage_path,
                  stage_directory_needs_deleting, &staged_directory, stream);
        if (!stage_status.ok()) {
            return stage_status;
        }
    }

    // The staging operation was successful.
    // Now we wait for a second request, which we expect to be empty:
    StageTreeRequest cleanup_request;
    const bool received_cleanup_request = stream->Read(&cleanup_request);

    if (received_cleanup_request &&
        (!cleanup_request.path().empty() ||
         !(cleanup_request.root_digest() == Digest()))) {
        std::ostringstream error_message;
        error_message << "Unexpected non-empty request after staging "
                      << stage_request.root_digest() << " in " << stage_path;
        stage_status =
            Status(grpc::StatusCode::INVALID_ARGUMENT, error_message.str());
    }

    // The `StagedDirectory` object's destructor will perform the
    // unstaging, clearing the directory's contents.
    staged_directory.reset();

    try {
        // If we created the top-level directory, we delete it as well.
        if (stage_directory_needs_deleting) {
            buildboxcommon::FileUtils::deleteDirectory(stage_path.c_str());
        }
    }
    catch (const std::system_error &e) {
        BUILDBOX_LOG_WARNING("Caught " << e.what()
                                       << " while removing directory ["
                                       << stage_path);
    }

    // Sending an empty reply that signals that we are done cleaning:
    if (received_cleanup_request) {
        stream->Write(StageTreeResponse());
    }

    return stage_status;
}

bool LocalCasInstance::treeIsAvailableLocally(const Digest &root_digest,
                                              bool file_blobs) const
{
    int64_t cache_hits = 0;
    int64_t cache_misses = 0;

    const bool ret = treeIsAvailableLocally(root_digest, file_blobs,
                                            &cache_hits, &cache_misses);

    recordTreeCacheMetrics(cache_hits, cache_misses);

    return ret;
}

bool LocalCasInstance::treeIsAvailableLocally(const Digest &root_digest,
                                              bool file_blobs,
                                              int64_t *cache_hits,
                                              int64_t *cache_misses) const
{
    auto tree_cache = this->getTreeCache();
    if (tree_cache->hasRootDigest(root_digest, file_blobs)) {
        (*cache_hits)++;
        return true;
    }

    (*cache_misses)++;

    Directory directory;
    if (!d_storage->hasBlob(root_digest) ||
        !directory.ParseFromString(*d_storage->readBlob(root_digest))) {
        return false;
    }

    if (file_blobs) {
        for (const FileNode &file : directory.files()) {
            if (!this->d_storage->hasBlob(file.digest())) {
                return false;
            }
        }
    }

    for (const DirectoryNode &dir : directory.directories()) {
        if (!treeIsAvailableLocally(dir.digest(), file_blobs, cache_hits,
                                    cache_misses)) {
            return false;
        }
    }

    tree_cache->addRootDigest(root_digest, file_blobs);

    return true;
}

Status LocalCasInstance::prepareTreeForStaging(const Digest &root_digest) const
{
    // Server mode. (All the blobs must be available locally.)
    if (treeIsAvailableLocally(root_digest)) {
        return grpc::Status::OK;
    }
    return Status(grpc::StatusCode::FAILED_PRECONDITION,
                  "Tree is not completely available from LocalCAS.");
}

std::vector<Digest>
LocalCasInstance::digestsMissingFromDirectory(const Directory &directory,
                                              bool file_blobs) const
{
    std::vector<Digest> missing_digests;
    missing_digests.reserve(static_cast<size_t>(directory.files_size()) +
                            static_cast<size_t>(directory.directories_size()));

    if (file_blobs) {
        for (const FileNode &file : directory.files()) {
            if (!this->d_storage->hasBlob(file.digest())) {
                missing_digests.push_back(file.digest());
            }
        }
    }
    for (const DirectoryNode &dir : directory.directories()) {
        if (!this->d_storage->hasBlob(dir.digest())) {
            missing_digests.push_back(dir.digest());
        }
    }
    missing_digests.shrink_to_fit();
    return missing_digests;
}

Status LocalCasInstance::stage(
    const Digest &root_digest, const std::string &stage_path,
    bool delete_directory_on_error,
    std::unique_ptr<FileStager::StagedDirectory> *staged_directory,
    ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
{
    Status stage_status;
    try {

        *staged_directory = d_file_stager->stage(root_digest, stage_path);

        StageTreeResponse response;
        response.set_path(stage_path);
        stream->Write(response);

        stage_status = grpc::Status::OK;
    }
    catch (const std::invalid_argument &e) {
        stage_status = Status(grpc::StatusCode::INVALID_ARGUMENT, e.what());
    }
    catch (const std::runtime_error &e) {
        stage_status = Status(grpc::StatusCode::INTERNAL, e.what());
    }

    if (!stage_status.ok() && delete_directory_on_error) {
        // `stage()` rolls back the status of `stage_path` when the
        // operation aborts, leaving it as it originally was.
        // However, if we created a temporary directory, we want to delete
        // it.
        try {
            buildboxcommon::FileUtils::deleteDirectory(stage_path.c_str());
        }
        catch (const std::system_error &e) {
            BUILDBOX_LOG_ERROR("Could not delete directory "
                               << stage_path << ":" << e.what());
        }
    }

    return stage_status;
}

std::string LocalCasInstance::createStagingDirectory() const
{
    buildboxcommon::TemporaryDirectory stage_directory =
        d_storage->createStagingDirectory();
    stage_directory.setAutoRemove(false);
    return std::string(stage_directory.name());
}

Digest LocalCasInstance::captureFile(int fd)
{
    Digest digest;
    d_storage->writeBlob(fd, &digest);

    return digest;
}

bool LocalCasInstance::hasRemote() const { return false; }

Status LocalCasInstance::CaptureTree(const CaptureTreeRequest &request,
                                     CaptureTreeResponse *response)
{
    const std::vector<std::string> capture_properties(
        request.node_properties().begin(), request.node_properties().end());

    const bool upload = hasRemote();
    const bool bypass_local_cache = upload && request.bypass_local_cache();

    for (const std::string &path : request.path()) {
        auto entry = response->add_responses();
        *entry = captureDirectory(path, capture_properties, bypass_local_cache,
                                  request.move_files());
    }

    return grpc::Status::OK;
}

CaptureTreeResponse_Response LocalCasInstance::captureDirectory(
    const std::string &path,
    const std::vector<std::string> &capture_properties,
    const bool bypass_local_cache, const bool move_files_hint)
{
    CaptureTreeResponse_Response response;

    google::rpc::Status status;

    if (path.empty() || !FileUtils::isDirectory(path.c_str()) ||
        path[0] != '/') {
        const auto error_message =
            "Path: " + path +
            " is empty, not a directory, or not an absolute "
            "path.";
        logAndStoreMessage(grpc::StatusCode::NOT_FOUND, error_message,
                           &status);
        *response.mutable_status() = std::move(status);
        return response;
    }

    digest_string_map digest_map;
    Tree tree_message;
    Digest tree_digest;

    try {
        NestedDirectory nested_directory;
        if (!bypass_local_cache && !move_files_hint) {
            nested_directory = make_nesteddirectory(
                path.c_str(), [&](int fd) { return captureFile(fd); },
                &digest_map, capture_properties);
        }
        else {
            nested_directory = make_nesteddirectory(path.c_str(), &digest_map,
                                                    capture_properties);
        }

        tree_message = nested_directory.to_tree();
        nested_directory.to_digest(&digest_map);
    }
    catch (const OutOfSpaceException &e) {
        const auto error_message =
            "Out of space error in `captureFile()` for path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::RESOURCE_EXHAUSTED, error_message,
                           &status);
        *response.mutable_status() = std::move(status);
        return response;
    }
    // Catch system errors thrown in `make_nesteddirectory()`.
    catch (const std::system_error &e) {
        const auto error_message =
            "System error in `make_nesteddirectory()` for path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::INTERNAL, error_message, &status);
        *response.mutable_status() = std::move(status);
        return response;
    }

    // Upload to remote CAS and store locally if `bypass_local_cache` is
    // not set
    try {
        tree_digest = UploadAndStore(digest_map, &tree_message,
                                     bypass_local_cache, move_files_hint);

        const auto number_of_blobs_captured = static_cast<
            buildboxcommon::buildboxcommonmetrics::CountingMetricValue::Count>(
            digest_map.size());
        buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
            recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_CAPTURED,
                                number_of_blobs_captured);

        status.set_code(grpc::StatusCode::OK);
    }
    catch (const OutOfSpaceException &e) {
        const auto error_message =
            "Out of space error in `writeBlob()` for path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::RESOURCE_EXHAUSTED, error_message,
                           &status);
    }
    catch (const std::runtime_error &e) {
        const auto error_message =
            "Runtime error uploading and storing path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::INTERNAL, error_message, &status);
    }
    catch (const std::invalid_argument &e) {
        const auto error_message =
            "Invalid argument error uploading and storing path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::INTERNAL, error_message, &status);
    }

    if (status.code() == grpc::StatusCode::OK) {
        response.mutable_tree_digest()->CopyFrom(tree_digest);
        response.set_path(path);
    }

    *response.mutable_status() = std::move(status);
    return response;
}

Status LocalCasInstance::CaptureFiles(const CaptureFilesRequest &request,
                                      CaptureFilesResponse *response)
{
    // Currently only support MTime but send all properties to File constructor
    const std::vector<std::string> capture_properties(
        request.node_properties().cbegin(), request.node_properties().cend());
    // used to avoid loop later
    const bool capture_mtime =
        (std::find(capture_properties.cbegin(), capture_properties.cend(),
                   "mtime") != capture_properties.cend());

    const bool upload = hasRemote();
    const bool bypass_local_cache = upload && request.bypass_local_cache();

    for (const std::string &path : request.path()) {
        auto entry = response->add_responses();
        *entry = captureFile(path, capture_properties, capture_mtime,
                             bypass_local_cache, request.move_files());
    }

    return grpc::Status::OK;
}

google::rpc::Status
LocalCasInstance::captureFile(const File &file, const std::string &path,
                              const bool bypass_local_cache,
                              const bool move_file)
{
    google::rpc::Status status;

    try {
        captureFile(file.d_digest, path, bypass_local_cache, move_file);

        const auto number_of_blobs_captured = static_cast<
            buildboxcommon::buildboxcommonmetrics::CountingMetricValue::Count>(
            1);

        buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
            recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_CAPTURED,
                                number_of_blobs_captured);

        status.set_code(grpc::StatusCode::OK);
    }
    catch (const OutOfSpaceException &e) {
        const auto error_message =
            "Out of space error in `writeBlob()` for path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::RESOURCE_EXHAUSTED, error_message,
                           &status);
    }
    catch (const std::runtime_error &e) {
        const auto error_message =
            "Runtime error uploading and storing path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::INTERNAL, error_message, &status);
    }
    catch (const std::invalid_argument &e) {
        const auto error_message =
            "Invalid argument error uploading and storing path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::INTERNAL, error_message, &status);
    }

    return status;
}

void LocalCasInstance::captureFile(const Digest &digest,
                                   const std::string &path, const bool,
                                   const bool move_file_hint)
{
    return addCapturedFileToLocalStorage(digest, path, move_file_hint);
}

void LocalCasInstance::addCapturedFileToLocalStorage(const Digest &digest,
                                                     const std::string &path,
                                                     const bool move_file_hint)
{
    if (move_file_hint && d_storage->externalFileMovesAllowed()) {
        try {
            if (d_storage->moveBlobFromExternalFile(digest, path)) {
                return;
            }
        }
        catch (const std::runtime_error &e) {
            BUILDBOX_LOG_DEBUG("Moving blob with digest ["
                               << digest
                               << "] failed (will try copying it instead): "
                               << e.what());
        }
        // If moving did not work for any reason, fall back to copying.
    }

    copyToLocalStorage(digest, path);
}

CaptureFilesResponse_Response LocalCasInstance::captureFile(
    const std::string &path,
    const std::vector<std::string> &capture_properties,
    const bool capture_mtime, const bool bypass_local_cache,
    const bool move_file_hint)
{
    CaptureFilesResponse_Response response;

    if (path.empty() || FileUtils::isDirectory(path.c_str())) {
        google::rpc::Status status;
        const auto error_message =
            "Path: " + path + " is empty, or is a directory.";
        logAndStoreMessage(grpc::StatusCode::NOT_FOUND, error_message,
                           &status);
        *response.mutable_status() = std::move(status);
        return response;
    }

    File file;
    try {
        if (!bypass_local_cache && !move_file_hint) {
            file = File(path.c_str(), [&](int fd) { return captureFile(fd); },
                        capture_properties);
        }
        else {
            file = File(path.c_str(), capture_properties);
        }
    }
    catch (const OutOfSpaceException &e) {
        google::rpc::Status status;
        const auto error_message =
            "Out of space error in `captureFile()` for path \"" + path +
            "\": " + e.what();
        logAndStoreMessage(grpc::StatusCode::RESOURCE_EXHAUSTED, error_message,
                           &status);
        *response.mutable_status() = std::move(status);
        return response;
    }
    catch (const std::runtime_error &e) {
        google::rpc::Status status;
        const auto error_message =
            std::string(e.what()) + " thrown for path: " + path;
        logAndStoreMessage(grpc::StatusCode::INTERNAL, error_message, &status);
        *response.mutable_status() = std::move(status);
        return response;
    }

    *response.mutable_status() =
        captureFile(file, path, bypass_local_cache, move_file_hint);

    if (response.status().code() == grpc::StatusCode::OK) {
        response.mutable_digest()->CopyFrom(file.d_digest);
        response.set_path(path);
        response.set_is_executable(file.d_executable);

        if (capture_mtime) {
            *response.mutable_node_properties()->mutable_mtime() =
                TimeUtils::make_timestamp(file.d_mtime);
        }
    }

    return response;
}

void LocalCasInstance::copyToLocalStorage(const Digest &digest,
                                          const std::string &path)
{
    if (d_storage->hasBlob(digest)) {
        return;
    }

    int fd = open(path.c_str(), O_RDONLY);
    if (fd < 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(std::system_error, errno,
                                              std::system_category,
                                              "Could not open file " << path);
    }

    Digest computed_digest;
    try {
        d_storage->writeBlob(fd, &computed_digest);
    }
    catch (...) {
        close(fd);
        throw;
    }
    close(fd);

    if (digest != computed_digest) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::invalid_argument,
            "Digest " << digest << " does not match data (expected "
                      << computed_digest << ")");
    }
}

Digest LocalCasInstance::UploadAndStore(
    const buildboxcommon::digest_string_map &digest_map, const Tree *t,
    const bool, const bool move_files_hint)
{
    for (const auto &it : digest_map) {
        const Digest &digest = it.first;
        // The map values can be either Directory messages or a paths to files.

        Directory d;
        if (d.ParseFromString(it.second)) {
            d_storage->writeBlob(digest, it.second);
        }
        else {
            const std::string &path = it.second;
            addCapturedFileToLocalStorage(digest, path, move_files_hint);
        }
    }

    Digest tree_digest;
    if (t != nullptr) {
        // Write the `Tree` message as well:
        tree_digest = buildboxcommon::make_digest(*t);
        d_storage->writeBlob(tree_digest, t->SerializeAsString());
    }

    return tree_digest;
}

Status LocalCasInstance::FetchTree(const FetchTreeRequest &request,
                                   FetchTreeResponse *)
{
    buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
        MetricNames::COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_REQUESTS, 1);
    if (treeIsAvailableLocally(request.root_digest(),
                               request.fetch_file_blobs())) {
        return grpc::Status::OK;
    }
    else {
        return grpc::Status(
            grpc::StatusCode::NOT_FOUND,
            "The tree is not available in the local CAS or incomplete.");
    }
}
