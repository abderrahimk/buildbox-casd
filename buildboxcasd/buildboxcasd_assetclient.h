/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_ASSETCLIENT_H
#define INCLUDED_BUILDBOXCASD_ASSETCLIENT_H

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_protos.h>

namespace buildboxcasd {

using namespace build::bazel::remote::asset::v1;

/**
 * Implements a mechanism to communicate with Remote Asset servers.
 */
class AssetClient final {
  public:
    AssetClient(){};

    /**
     * Connect to the Remote Asset server with the given connection options.
     */
    void init(const buildboxcommon::ConnectionOptions &options);

    /**
     * Connect to the Remote Asset server with the given clients.
     */
    void init(std::shared_ptr<Fetch::StubInterface> fetchClient,
              std::shared_ptr<Push::StubInterface> pushClient);

    std::string instanceName() const;

    void setInstanceName(const std::string &instance_name);

    void fetchBlob(const FetchBlobRequest &request,
                   FetchBlobResponse *response);

    void fetchDirectory(const FetchDirectoryRequest &request,
                        FetchDirectoryResponse *response);

    void pushBlob(const PushBlobRequest &request);

    void pushDirectory(const PushDirectoryRequest &request);

  private:
    std::shared_ptr<grpc::Channel> d_channel;
    std::shared_ptr<Fetch::StubInterface> d_fetchClient;
    std::shared_ptr<Push::StubInterface> d_pushClient;

    int d_grpcRetryLimit = 4;
    int d_grpcRetryDelay = 1000;

    std::string d_instanceName;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_ASSETCLIENT_H
