﻿/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_assetclient.h>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcretry.h>
#include <buildboxcommon_protos.h>

#include <string>

namespace buildboxcasd {

using namespace build::bazel::remote::asset::v1;

void AssetClient::init(const buildboxcommon::ConnectionOptions &options)
{
    std::shared_ptr<grpc::Channel> channel = options.createChannel();
    this->d_grpcRetryLimit = std::stoi(options.d_retryLimit);
    this->d_grpcRetryDelay = std::stoi(options.d_retryDelay);
    this->d_channel = channel;
    this->d_instanceName = options.d_instanceName;

    std::shared_ptr<Fetch::Stub> fetchClient = Fetch::NewStub(this->d_channel);
    std::shared_ptr<Push::Stub> pushClient = Push::NewStub(this->d_channel);
    init(fetchClient, pushClient);
}

void AssetClient::init(std::shared_ptr<Fetch::StubInterface> fetchClient,
                       std::shared_ptr<Push::StubInterface> pushClient)
{
    this->d_fetchClient = fetchClient;
    this->d_pushClient = pushClient;
}

std::string AssetClient::instanceName() const { return d_instanceName; }

void AssetClient::setInstanceName(const std::string &instance_name)
{
    d_instanceName = instance_name;
}

void AssetClient::fetchBlob(const FetchBlobRequest &request,
                            FetchBlobResponse *response)
{
    auto fetchLambda = [&](grpc::ClientContext &context) {
        FetchBlobRequest remoteRequest;
        remoteRequest.CopyFrom(request);
        remoteRequest.set_instance_name(d_instanceName);
        return d_fetchClient->FetchBlob(&context, remoteRequest, response);
    };
    buildboxcommon::GrpcRetry::retry(fetchLambda, this->d_grpcRetryLimit,
                                     this->d_grpcRetryDelay);
}

void AssetClient::fetchDirectory(const FetchDirectoryRequest &request,
                                 FetchDirectoryResponse *response)
{
    auto fetchLambda = [&](grpc::ClientContext &context) {
        FetchDirectoryRequest remoteRequest;
        remoteRequest.CopyFrom(request);
        remoteRequest.set_instance_name(d_instanceName);
        return d_fetchClient->FetchDirectory(&context, remoteRequest,
                                             response);
    };
    buildboxcommon::GrpcRetry::retry(fetchLambda, this->d_grpcRetryLimit,
                                     this->d_grpcRetryDelay);
}

void AssetClient::pushBlob(const PushBlobRequest &request)
{
    auto pushLambda = [&](grpc::ClientContext &context) {
        PushBlobRequest remoteRequest;
        PushBlobResponse response;
        remoteRequest.CopyFrom(request);
        remoteRequest.set_instance_name(d_instanceName);
        return d_pushClient->PushBlob(&context, remoteRequest, &response);
    };
    buildboxcommon::GrpcRetry::retry(pushLambda, this->d_grpcRetryLimit,
                                     this->d_grpcRetryDelay);
}

void AssetClient::pushDirectory(const PushDirectoryRequest &request)
{
    auto pushLambda = [&](grpc::ClientContext &context) {
        PushDirectoryRequest remoteRequest;
        PushDirectoryResponse response;
        remoteRequest.CopyFrom(request);
        remoteRequest.set_instance_name(d_instanceName);
        return d_pushClient->PushDirectory(&context, remoteRequest, &response);
    };
    buildboxcommon::GrpcRetry::retry(pushLambda, this->d_grpcRetryLimit,
                                     this->d_grpcRetryDelay);
}

} // namespace buildboxcasd
